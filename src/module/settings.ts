const MODULE_NAME = "quick-insert";
export const SAVE_SETTINGS_REVISION = 1;

type Callback = (value?: unknown) => void;

export function registerSetting(
  setting: string,
  callback: Callback,
  { ...options }: Record<string, unknown>
): void {
  game.settings.register(MODULE_NAME, setting, {
    config: true,
    scope: "client",
    ...options,
    onChange: callback || undefined,
  });
}

export function getSetting(setting: string): any {
  return game.settings.get(MODULE_NAME, setting as string);
}

export function setSetting(setting: string, value: unknown): Promise<unknown> {
  return game.settings.set(MODULE_NAME, setting as string, value);
}

export function registerMenu({
  menu,
  ...options
}: { menu: string } & ClientSettings.PartialMenuSetting): void {
  game.settings.registerMenu(MODULE_NAME, menu, options as any);
}
